/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author João Paulo
 */
public class Pratica {
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        
        System.out.println("nome do sistema operacional: "+(System.getProperty("os.name")));
        System.out.println("número de processadores: "+rt.availableProcessors());
        System.out.println("memória total em MB: "+rt.totalMemory());
        System.out.println("memória livre em MB: "+rt.freeMemory());
        System.out.println("máxima quantidade de memória usada pela máquina virtual em MB: "+rt.maxMemory()/1024/1024);
    }
}
